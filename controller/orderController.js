export const get = (req, res) => {
  return res.send('list Order');
};

export const create = (req, res) => {
  return res.send('create order');
};

export const update = (req, res) => {
  return res.send('update order');
};

export const destroy = (req, res) => {
  return res.send('delete order');
};

export const find = (req, res) => {
  return res.send('find order');
};
