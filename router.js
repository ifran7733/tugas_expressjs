import express from 'express';
import { checkHealth } from './controller/checkHealthController.js';
import { bio } from './controller/biodataController.js';
import { checkAuthKey } from './middleware/checkKey.js';
// cara 2
import productApi from './api/orderApi.js';
import bookApi from './api/bookApi.js';
const router = express.Router();

router.get('/check-health', checkHealth);
router.get('/biodata', checkAuthKey, bio);
router.use('/order', productApi);
router.use('/book', bookApi);

export default router;
